

/**
 * Celestial Body class for NBody
 * Modified from original Planet class
 * used at Princeton and Berkeley
 * @author ola
 *
 * If you add code here, add yourself as @author below
 *
 *
 */
public class CelestialBody {

	private double myXPos;
	private double myYPos;
	private double myXVel;
	private double myYVel;
	private double myMass;
	private String myFileName;

	/**
	 * Create a Body from parameters	
	 * @param xp initial x position
	 * @param yp initial y position
	 * @param xv initial x velocity
	 * @param yv initial y velocity
	 * @param mass of object
	 * @param filename of image for object animation
	 */
	public CelestialBody(double xp, double yp, double xv,
			             double yv, double mass, String filename){
		this.myXPos = xp;
		this.myYPos = yp; 
		this.myXVel = xv;
		this.myYVel = yv; 
		this.myMass = mass;
		this.myFileName = filename; 


	}


    /**
	 *
	 * @return
	 */
	public double getX() {
		return myXPos;
		//return 0.0;
	}

	/**
	 *
	 * @return
	 */
	public double getY() {
		return this.myYPos;
		//return 0.0;
	}

	/**
	 * Accessor for the x-velocity
	 * @return the value of this object's x-velocity
	 */
	public double getXVel() {
		return this.myXVel; 
		//return 0.0;
	}
	/**
	 * Return y-velocity of this Body.
	 * @return value of y-velocity.
	 */
	public double getYVel() {
		return this.myYVel; 
		//return 0.0;
	}

	/**
	 *
	 * @return
	 */
	public double getMass() {
		return this.myMass; 
		//return 5.0;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return this.myFileName; 
		//return "cow-planet";
	}

	/**
	 * Return the distance between this body and another
	 * @param b the other body to which distance is calculated
	 * @return distance between this body and b
	 */
	public double calcDistance(CelestialBody b) { 
		//return Math.sqrt(Math.abs(Math.pow((this.myXPos - b.myXPos), 2))) + (Math.abs(Math.pow((this.myYPos - b.myYPos), 2))); 
		 double diffX = Math.abs(Math.pow((this.myXPos - b.myXPos), 2)); 
		 double diffY = Math.abs(Math.pow((this.myYPos - b.myYPos), 2)); 
		 double r = Math.sqrt(diffX + diffY); 
		 return r; 

	}

	public double calcForceExertedBy(CelestialBody b) {
		// TODO: complete method
		double bothMass = this.myMass * b.myMass; 
		double rSquared = Math.pow(calcDistance(b), 2); 
		double g = (6.67*1e-11); 
		double div = bothMass / rSquared; 
		double f = g * div; 
		return f;   
		//return 0.0;
	}

	public double calcForceExertedByX(CelestialBody b) {
		// TODO: complete method
		double f = calcForceExertedBy(b);
		double dx = (b.myXPos - this.myXPos);   
		double r = calcDistance(b); 
		double fx = (f * (dx/r)); 
		return fx; 

	}
	public double calcForceExertedByY(CelestialBody b) {
		// TODO: complete method
		double f = calcForceExertedBy(b);
		double dy = (b.myYPos - this.myYPos);
		double r = calcDistance(b); 
		double fy = (f * (dy/r)); 
		return fy;
	}

	public double calcNetForceExertedByX(CelestialBody[] bodies) {
		// TODO: complete method
		double sum = 0.0; 
		for(CelestialBody b : bodies){
			if (!b.equals(this)){
				sum = sum + calcForceExertedByX(b);  
			}
		}
		return sum; 
	}

	public double calcNetForceExertedByY(CelestialBody[] bodies) {
		double sum = 0.0;
		for(CelestialBody b : bodies){
			if (!b.equals(this)){
				sum = sum + calcForceExertedByY(b);  
			}
		}
		return sum; 
	}

	public void update(double deltaT, 
			           double xforce, double yforce) {
		// TODO: complete method
			double accX = xforce / myMass; 
			double accY = yforce / myMass;  
			double nvx = myXVel+ deltaT*accX; 
			double nvy = myYVel + deltaT*accY; 
			double nx = myXPos + deltaT*nvx;
			double ny = myYPos + deltaT*nvy; 
			this.myXPos = nx;
			this.myYPos = ny; 
			this.myXVel = nvx;
			this.myYVel = nvy; 
		}

	

	/**
	 * Draws this planet's image at its current position
	 */
	public void draw() {
		StdDraw.picture(myXPos,myYPos,"images/"+myFileName);
	}

    private static CelestialBody[] readFile(String fname) {
        return null;
    }
}
